# frozen_string_literal: true

require 'test_helper'

class PlayerTest < ActiveSupport::TestCase
  def setup
    bono = 10_000
    equipo = "resuelve"
    goles = 19
    nombre = "Luis"
    sueldo = 50_000
    options = { nivel: "Cuauh", nombre: nombre }
    @player = Player.new(goles, sueldo, bono, equipo, options)
  end

  test 'should create player' do
    assert_equal @player.equipo, "resuelve"
    assert_equal @player.goles, 19
    assert_equal @player.nivel, "Cuauh"
    assert_equal @player.sueldo, 50_000
    assert_equal @player.nombre, "Luis"
  end

  test 'should calculate bono_variable_individual and goles_minimos por nivel' do
    assert_equal @player.goles_minimos, 20
    assert_equal @player.bono_variable_individual, 95
  end

  test 'should create a player with custom goles_minimos' do
    bono = 10_000
    equipo = "resuelve"
    goles = 19
    nombre = "Luis"
    sueldo = 50_000
    options = { goles_minimos: 19, nombre: nombre }

    player = Player.new(goles, sueldo, bono, equipo, options)

    assert_equal player.goles_minimos, 19
    assert_equal player.bono_variable_individual, 100
  end
end
