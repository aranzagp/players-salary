# frozen_string_literal: true

require 'test_helper'

class TeamTest < ActiveSupport::TestCase
  def setup
    equipo = "resuelve"
    @team = Team.new(equipo)

    bono = 10_000
    equipo = "resuelve"
    goles = 19
    nombre = "Luis"
    sueldo = 50_000
    options = { nivel: "Cuauh", nombre: nombre }
    player = Player.new(goles, sueldo, bono, equipo, options)
    player2 = Player.new(6, sueldo, bono, equipo, { nivel: "A", nombre: 'Juan' })
    player3 = Player.new(7, sueldo, bono, equipo, { nivel: "B", nombre: 'Pedro' })
    player4 = Player.new(16, sueldo, bono, equipo, { nivel: "C", nombre: 'Martin' })

    @team.add(player)
    @team.add(player2)
    @team.add(player3)
    @team.add(player4)
  end

  test 'should create a team' do
    assert_equal @team.team_name, "resuelve"
  end

  test 'shouls add players to a team' do
    assert @team.players.count, 4
    assert @team.players[0], @team.player(0)
  end

  test 'shouls calculate bono_por_equipo' do
    assert @team.bono_por_equipo, 95.5
  end
end
