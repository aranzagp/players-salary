# frozen_string_literal: true

require "test_helper"

class PlayerBuilderTest < ActiveSupport::TestCase
  test "should create the resuelve players with the correct data" do
    json_players = IO.read('./test/fixtures/files/resuelve_players.json')
    teams = TeamBuilder.from_json(json_players)
    players = PlayerBuilder.from_teams(teams)

    output = [
      { nombre: "Juan", goles: 6, sueldo: 50_000, bono: 25_000, equipo: "resuelve", sueldo_completo: 77_000.0, nivel: "A" },
      { nombre: "Pedro", goles: 7, sueldo: 100_000, bono: 30_000, equipo: "resuelve", sueldo_completo: 124_900.0, nivel: "B" },
      { nombre: "Martín", goles: 16, sueldo: 20_000, bono: 10_000, equipo: "resuelve", sueldo_completo: 30_133.333333333336,
        nivel: "C" },
      { nombre: "Luis", goles: 19, sueldo: 50_000, bono: 10_000, equipo: "resuelve", sueldo_completo: 59_550.0, nivel: "Cuauh" }
    ]

    assert_equal players, output
  end

  test 'should create different team players with the correct data' do
    json_players = IO.read('./test/fixtures/files/mixed_teams_players.json')
    teams = TeamBuilder.from_json(json_players)
    players = PlayerBuilder.from_teams(teams)

    output = [
      { nombre: "Juan Perez", goles: 10, sueldo: 50_000, bono: 25_000, equipo: "rojo", sueldo_completo: 67_833.33333333333,
        nivel: "C" },
      { nombre: "El Rulo", goles: 9, sueldo: 30_000, bono: 15_000, equipo: "rojo", sueldo_completo: 42_450.0, nivel: "B" },
      { nombre: "EL Cuauh", goles: 30, sueldo: 100_000, bono: 30_000, equipo: "azul", sueldo_completo: 144_700.0,
        nivel: "Cuauh" },
      { nombre: "Cosme Fulanito", goles: 7, sueldo: 20_000, bono: 10_000, equipo: "azul", sueldo_completo: 34_400.0, nivel: "A" }
    ]

    assert_equal players, output
  end

  test 'should create different team players with goles_minimos with the correct data' do
    json_players = IO.read('./test/fixtures/files/mixed_teams_players_goles_minimos.json')
    teams = TeamBuilder.from_json(json_players)
    players = PlayerBuilder.from_teams(teams)

    output = [
      { nombre: "Juan Perez", goles: 10, sueldo: 50_000, bono: 25_000, equipo: "rojo", sueldo_completo: 75_000.0,
        goles_minimos: 10 },
      { nombre: "El Rulo", goles: 9, sueldo: 30_000, bono: 15_000, equipo: "rojo", sueldo_completo: 45_000.0, goles_minimos: 9 },
      { nombre: "EL Cuauh", goles: 30, sueldo: 100_000, bono: 30_000, equipo: "azul", sueldo_completo: 130_000.0,
        goles_minimos: 30 },
      { nombre: "Cosme Fulanito", goles: 7, sueldo: 20_000, bono: 10_000, equipo: "azul", sueldo_completo: 30_000.0,
        goles_minimos: 7 }
    ]

    assert_equal players, output
  end
end
