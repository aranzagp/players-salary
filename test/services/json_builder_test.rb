# frozen_string_literal: true

require "test_helper"

class JsonBuilderTest < ActiveSupport::TestCase
  test "should create the resuelve players json with the correct data" do
    json_players = IO.read('./test/fixtures/files/resuelve_players.json')
    players = JsonBuilder.run(json_players)

    output = { jugadores: [
      { nombre: "Juan", goles: 6, sueldo: 50_000, bono: 25_000, equipo: "resuelve", sueldo_completo: 77_000.0, nivel: "A" },
      { nombre: "Pedro", goles: 7, sueldo: 100_000, bono: 30_000, equipo: "resuelve", sueldo_completo: 124_900.0, nivel: "B" },
      { nombre: "Martín", goles: 16, sueldo: 20_000, bono: 10_000, equipo: "resuelve", sueldo_completo: 30_133.333333333336,
        nivel: "C" },
      { nombre: "Luis", goles: 19, sueldo: 50_000, bono: 10_000, equipo: "resuelve", sueldo_completo: 59_550.0, nivel: "Cuauh" }
    ] }

    assert_equal players, output.to_json
  end

  test "should raise an error with an invalid json" do
    json_players = IO.read('./test/fixtures/files/invalid.json')
    players = JsonBuilder.run(json_players)

    formato1 = {
      jugadores: [
        {
          nombre: "El Rulo",
          nivel: "A",
          goles: 9,
          sueldo: 30_000,
          bono: 15_000,
          sueldo_completo: nil,
          equipo: "rojo"
        }
      ]
    }

    formato2 = {
      jugadores: [
        {
          nombre: "El Rulo",
          goles_minimos: 10,
          goles: 9,
          sueldo: 30_000,
          bono: 15_000,
          sueldo_completo: nil,
          equipo: "rojo"
        }
      ]
    }

    output = { error: "Tu archivo JSON es invalido: verifica que tenga los siguientes formatos #{formato1} o #{formato2}" }

    assert_equal output, players
  end
end
