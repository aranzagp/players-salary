# frozen_string_literal: true

require "test_helper"

class TeamBuilderTest < ActiveSupport::TestCase
  test "should create the resuelve team" do
    json_players = IO.read('./test/fixtures/files/resuelve_players.json')
    teams = TeamBuilder.from_json(json_players)

    assert teams.size, 1
    assert_equal teams['resuelve'].class, Team
    assert_equal teams['resuelve'].players.size, 4
    assert_equal teams['resuelve'].bono_por_equipo, 96
  end

  test 'should create different teams' do
    json_players = IO.read('./test/fixtures/files/mixed_teams_players.json')
    teams = TeamBuilder.from_json(json_players)
    red_team = teams['rojo']
    blue_team = teams['azul']

    assert teams.size, 2
    assert_equal red_team.class, Team
    assert_equal blue_team.class, Team

    assert_equal red_team.players.size, 2
    assert_equal blue_team.players.size, 2

    # 76 = (10.0 + 9)/(15 + 10) * 100
    bono_rojo = (red_team.player(0).goles.to_f + red_team.player(1).goles) /
                (red_team.player(0).goles_minimos + red_team.player(1).goles_minimos) * 100
    bono_azul = (blue_team.player(0).goles.to_f + blue_team.player(1).goles) /
                (blue_team.player(0).goles_minimos + blue_team.player(1).goles_minimos) * 100

    assert_equal red_team.bono_por_equipo, bono_rojo
    assert_equal blue_team.bono_por_equipo, bono_azul
  end
end
