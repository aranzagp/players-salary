ENV['RAILS_ENV'] ||= 'test'

require 'simplecov'
SimpleCov.start 'rails'
SimpleCov.minimum_coverage 90
SimpleCov.add_filter ['vendor']
SimpleCov.add_filter do |src|
  ['application_job.rb', 'application_mailer.rb', 'application_record.rb'].member? File.basename(src.filename)
end

require_relative "../config/environment"
require "rails/test_help"
require 'minitest/unit'
require 'mocha/minitest'

class ActiveSupport::TestCase
  # Run tests in parallel with specified workers
  parallelize(workers: 1)

  # Setup all fixtures in test/fixtures/*.yml for all tests in alphabetical order.
  fixtures :all

  parallelize_setup do |worker|
    SimpleCov.command_name("#{SimpleCov.command_name}-#{worker}")
    SimpleCov.pid = Process.pid
    SimpleCov.at_exit {} # This will suppress formatters running at the end of each fork
  end

  parallelize_teardown do |_worker|
    SimpleCov.result
  end
end
