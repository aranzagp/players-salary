# frozen_string_literal: true

class Player < Team
  attr_accessor :parent
  attr_reader :nombre, :nivel, :goles, :sueldo, :bono, :equipo

  GOLES_MINIMOS_REQUERIDOS_POR_NIVEL = {
    'A' => 5,
    'B' => 10,
    'C' => 15,
    'Cuauh' => 20
  }.freeze

  def initialize(goles, sueldo, bono, equipo, options = {})
    super(equipo)
    @goles = goles
    @sueldo = sueldo
    @bono = bono
    @equipo = equipo
    @nombre = options[:nombre]
    @goles_minimos = options[:goles_minimos]
    @nivel = options[:nivel]
  end

  def bono_variable_individual
    (@goles.to_f / goles_minimos) * 100
  end

  def goles_minimos
    (GOLES_MINIMOS_REQUERIDOS_POR_NIVEL[@nivel] || @goles_minimos)
  end
end
