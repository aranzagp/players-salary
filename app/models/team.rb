class Team
  attr_reader :players, :team_name

  def initialize(equipo)
    @team_name = equipo
    @players = []
  end

  def add(player)
    @players.push(player)
    player.parent = self
  end

  def player(index)
    @players[index]
  end

  def bono_por_equipo
    total_goles = players.sum(&:goles).to_f
    total_goles_minimos = players.sum(&:goles_minimos).to_f
    @bono_por_equipo = (total_goles / total_goles_minimos) * 100
  end
end
