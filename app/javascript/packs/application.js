// This file is automatically compiled by Webpack, along with any other files
// present in this directory. You're encouraged to place your actual application logic in
// a relevant structure within app/javascript and only use these pack files to reference
// that code so it'll be compiled.

import Rails from "@rails/ujs"
import Turbolinks from "turbolinks"
import * as ActiveStorage from "@rails/activestorage"
import "channels"

Rails.start()
Turbolinks.start()
ActiveStorage.start()

document.addEventListener('turbolinks:load', function() {
  const attachmentBtn = document.getElementById('file')
  console.log('CATGA')
  attachmentBtn.addEventListener('change', function(e) {
    console.log(e)
    const submit = document.getElementById('submit')
    if (e.target.files) {
      submit.classList.remove('disabled')
      submit.classList.remove('disable-click')}
    else{
      submit.classList.add('disabled')
      submit.classList.add('disable-click')
    }
  })
})