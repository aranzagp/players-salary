# frozen_string_literal: true

class TeamBuilder
  def self.from_json(json)
    json = JSON.parse(json)
    teams = {}
    json['jugadores'].each do |player|
      team_name = player['equipo']
      teams[team_name] = Team.new(team_name) unless teams[team_name]
      p = create_player(player, team_name)
      teams[team_name].add(p)
    end
    teams
  end

  def self.create_player(player, team_name)
    goles = player['goles']
    sueldo = player['sueldo']
    bono = player['bono']
    equipo = team_name
    options = {
      nombre: player['nombre'],
      goles_minimos: player["goles_minimos"],
      nivel: player['nivel']
    }
    Player.new(goles, sueldo, bono, equipo, options)
  end
end
