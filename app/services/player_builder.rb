# frozen_string_literal: true

class PlayerBuilder
  def self.from_teams(teams)
    players = []
    teams.each do |_team, attributes|
      attributes.players.each do |player|
        map = player_map(player)
        players << map
      end
    end
    players
  end

  def self.player_map(player)
    map = {
      nombre: player.nombre,
      goles: player.goles,
      sueldo: player.sueldo,
      bono: player.bono,
      equipo: player.equipo,
      sueldo_completo: sueldo_completo(player, player.bono)
    }
    if player.nivel
      map[:nivel] = player.nivel
    else
      map[:goles_minimos] = player.goles_minimos
    end
    map
  end

  def self.sueldo_completo(player, bono)
    player.sueldo + bono_variable(player, bono)
  end

  def self.bono_variable(player, bono)
    (player.bono_variable_individual + player.parent.bono_por_equipo) / 2 * bono / 100
  end
end
