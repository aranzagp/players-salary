# frozen_string_literal: true

class JsonBuilder
  def self.run(json)
    teams = TeamBuilder.from_json(json)
    players = PlayerBuilder.from_teams(teams)
    { jugadores: players }.to_json
  rescue StandardError
    formato1 = {
      jugadores: [
        {
          nombre: "El Rulo",
          nivel: "A",
          goles: 9,
          sueldo: 30_000,
          bono: 15_000,
          sueldo_completo: nil,
          equipo: "rojo"
        }
      ]
    }

    formato2 = {
      jugadores: [
        {
          nombre: "El Rulo",
          goles_minimos: 10,
          goles: 9,
          sueldo: 30_000,
          bono: 15_000,
          sueldo_completo: nil,
          equipo: "rojo"
        }
      ]
    }

    { error: "Tu archivo JSON es invalido: verifica que tenga los siguientes formatos #{formato1} o #{formato2}" }
  end
end
