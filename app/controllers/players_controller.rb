class PlayersController < ApplicationController
  before_action :validate_json, only: :create

  def new
    @json = params[:json]
  end

  def create
    @json = JsonBuilder.run(json_path)

    redirect_to root_path(json: @json)
  end

  private

  def json_path
    @json_path ||= IO.read(file_params.tempfile.path)
  end

  def file_params
    params.require(:file)
  end

  def validate_json
    redirect_to root_path(json: { error: 'Tu archivo debe ser .json' }) unless file_params.tempfile.path.end_with?('.json')
  end
end
