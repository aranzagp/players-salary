# Players Salary Calculator

This guide is meant to help you get up and running on development.

## Versions
- Ruby 3.0.0
- Rails 6.1.3

## Dependencies

- Postgres
- yarn


## Bundle
Run:

```
bundle install
```

## Datbabase

Run:

```
rails db:create
```

## Start the server

Run: 

```
bin/rails s
bin/webpack-dev-server
```

Open your browser on: `http://localhost:3000/`

## Run the tests

```
rails test
```

## Before pushing changes

We have `rubocop` for code quality.

```
bundle exec rubocop
```
